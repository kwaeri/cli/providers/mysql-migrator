/******************************************************************************
/**
 * SPDX-PackageName: kwaeri/mysql-migrator
 * SPDX-PackageVersion: 0.7.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES


// DEFINES


class AddUsersToCoreAPIMigration {
    /**
     * Class constructor
     */
    constructor(){}


    /**
     * Defines the process or query which will apply a 'migration' - or
     * set of changes -  to a database.
     *
     * @returns { Promise<MigrationPromise> }
     */
    up() {
        const ci = this;
        return new Promise (
            ( resolve, reject ) => {
                ci.dbo
                .query(   // Supply/Update your query for 'up' here:
                    `create table if not exists users ` +
                    `( id int(11) not null auto_increment, ` +
                    `guid binary(16) not null, ` +
                    `type int(11) not null, ` +
                    `acl int(11) not null, ` +
                    `ts datetime not null, ` +
                    `luts datetime not null, ` +
                    `luby binary(16) null, ` +
                    `verified int not null, ` +
                    //`username varchar(255) not null, ` +
                    `username varchar(255) null, ` +
                    `password text not null, ` +
                    `first varchar(255) null, ` +
                    `middle varchar(255) null, ` +
                    `last varchar(255) null, ` +
                    `email text not null, ` +
                    `context binary(16) null, ` +
                    `last_login datetime null, ` +
                    `last_failed_login datetime null, ` +
                    `failed_interval int not null, ` +
                    `primary key (guid), ` +
                    `unique key (username), ` +
                    //`unique key (email), ` +
                    `unique key (id), ` +
                    `index (username) );`
                )
                .then(
                    ( migrated ) => {
                        if( !migrated || !migrated.rows ) {
                            reject( new Error( `[MIGRATION][111320190939_AddUsersToCoreAPI]: There was an issue applying the migration: ` ) );
                        }

                        resolve( { result: true, ...migrated } );
                    }
                );
            }
        );
    }


    /**
     * Defines the process or query which will revert a 'migration' - or
     * set of changes - from a database.
     *
     * @returns { Promise<MigrationPromise> }
     */
    down() {
        const ci = this;
        return new Promise(
            ( resolve, reject ) => {
                ci.dbo
                .query(   // Supply/Update your query for 'down' here:
                    `drop table if exists users;`
                )
                .then(
                    ( reverted ) => {
                        if( !reverted || !reverted.rows ) {
                            reject( new Error( `[MIGRATION][111320190939_AddUsersToCoreAPI]: There was an issue reverting the migration: ` ) );
                        }

                        resolve( { result: true, ...reverted } );
                    }
                );
            }
        );
    }
}


module.exports = exports = AddUsersToCoreAPIMigration;

