/**
 * SPDX-PackageName: kwaeri/mysql-migrator
 * SPDX-PackageVersion: 0.7.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import { MysqlMigrator } from '../src/mysql-migrator.mjs';
import { Progress } from '@kwaeri/progress';
import * as _fs from 'node:fs/promises';


// DEFINES
let MIGRATION_ENV = process.env.NODE_ENV || "default";

const progress  = new Progress({ spinner: true, spinAnim: "dots", percentage: false }),
      migrator  = new MysqlMigrator(
                        MIGRATION_ENV !== "test" ? progress.getHandler() : undefined,
                        { environment: MIGRATION_ENV }
                    );

// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'MySQLMigrator Functionality Test Suite',
    () => {

        describe(
            'Get Service Type Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                migrator.serviceType,
                                "Migrator Service"
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Get Service Provider Subscriptions Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( migrator.getServiceProviderSubscriptions() ),
                                JSON.stringify(
                                    {
                                        commands: {
                                            "migrations": false    // Specifications for the 'add' command, false if there are none
                                        },
                                        required: {
                                            "migrations": {

                                            }
                                        },
                                        optional: {
                                            "migrations": {    // Optional flags for 'migrations' comand
                                                "step-back": {              // step-back is not a specification, but an option specific to the command itself.
                                                    "for": false,           // Or false, if it's not related to an option/value, rather only to the specification/command.
                                                    "flag": false,          // True insists that no value is given. Its existance equates to <option>=1, the lack of its
                                                    "values": null          // existence is similar to <option>=0.
                                                },
                                            }
                                        }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Get Service Provider Help Text Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( migrator.getServiceProviderSubscriptionHelpText() ),
                                JSON.stringify(
                                    {
                                        helpText: {
                                            "commands": {       // ⇦ List help text for our commands
                                                "migrations": { // ⇦ For the migrations command
                                                    "description": "The 'migrations' command automates the handling of migrations for an existing project.",
                                                    "specifications": { // ⇦ For the specifications
                                                    },
                                                    "options": {    // ⇦ For the command (migrations in this case)
                                                        "optional": {   // ⇦ There are only optional options for commands (they'd otherwise be specs)
                                                            "command": {    // ⇦ For the command itself
                                                                "step-back": {  // ⇦ List options
                                                                    "description": "Specify the number of migrations to undo from those that are applied.",
                                                                    "values": null
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Install Migrations Test',
            () => {
                it(
                    'Should assert true, indicating that the migration system installed successfully and returned undefined.',
                    async () => {
                        const migrationConfiguration = `` +
                        `{\n` +
                        `    "version": "0.4.1",\n` +
                        `    "type": "mysql",\n` +
                        `    "table": "nodekit_migrations"\n` +
                        `}`;
                        const installed = await migrator.install( migrationConfiguration as any );
                        return Promise.resolve(
                            assert.strictEqual(
                                installed,
                                undefined
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Check Migrations Install Test',
            () => {
                it(
                    'Should assert true, indicating that the migration system was found to be installed and returned undefined.',
                    async () => {
                        return Promise.resolve(
                            assert.strictEqual
                            (
                                await migrator.checkInstall(),
                                undefined
                            )
                        );
                    }
                );
            }
        );


        describe(
            'MySQLMigrator renderService() Test',
            () => {
                it(
                    'Should return true, indicating that the test migration was applied.',
                    async () => {
                        if( MIGRATION_ENV !== "test" )
                            progress.init();

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( await migrator.renderService( { quest: "migrations", specification: "", subCommands: [], args: {}, version: "1.2.3" } ) ),
                                JSON.stringify( { result: true, type: "run_migrations" } )
                            )
                        );
                    }
                );
            }
        );


        describe(
            'MySQLMigrator renderService() Test II',
            () => {
                it(
                    'Should return true, indicating that the applied test migration was reverted.',
                    async () => {
                        if( MIGRATION_ENV !== "test" )
                            progress.init();

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( await migrator.renderService( { quest: "migrations", specification: "", subCommands: [], args: { "step-back": "1" }, version: "1.2.3" } ) ),
                                JSON.stringify( { result: true, type: "run_migrations" } )
                            )
                        );
                    }
                );
            }
        );
    }
);
